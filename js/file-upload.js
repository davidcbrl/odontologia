$(document).ready(function() {
   
    $('.avatar').on('click', function() {
        $('.file-upload').click();
    });

    $('.file-upload').on('change', function(){
        loadFile(this);
    });

    function loadFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.avatar').css('background-image', 'url(' + e.target.result + ')');
                $('.avatar').css('filter', 'none');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

});