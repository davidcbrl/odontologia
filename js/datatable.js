$(document).ready(function () {

    $('.alert-action').hide();

    tabela();

    $('#cadastrar').on('click', function () {
        var grr = 'GRR' + $('#input-grr').val();
        var nome = $('#input-nome').val();
        var turma = $('#input-turma-ano').val() + '/' + $('#input-turma-periodo').val();
        var email = $('#input-email').val();
        
        if (grr === 'GRR' || nome === '' || email === '') {
            $('.alert-danger').show(setTimeout(() => {
                $('.alert-danger').hide();
            }, 3000));
            return;
        }

        $('#alunos').DataTable().destroy();
        $('#alunos > tbody').empty();

        $('#alunos > tbody').append('' +
            '<tr role="row" class="odd">' +
                '<td class="sorting_1">' + grr +
                '<td>' + nome +
                '<td>' + turma +
            '</tr>' +
        '');

        tabela();
        
        $('.alert-success').show(setTimeout(() => {
            $('.alert-success').hide();
        }, 3000));
        $('#limpar').click();
        $('#painel-tab').click();
    });

});

function tabela() {
    $.getJSON('../js/alunos.json', function (data) {
        var count = 0;
        $.each(data, function (index, value) {
            $('#alunos > tbody').append('' +
                '<tr>' +
                    '<td>' + value.grr + '</td>' +
                    '<td>' + value.nome + '</td>' +
                    '<td>' + value.turma + '</td>' +
                '</tr>' +
            '');
            count++;
            if (count === data.length) {
                $('#alunos').DataTable({
                    lengthMenu: [ 5, 10, 20, 50 ],
                    searching: true,
                    fixedHeader: false,
                    ordering: true,
                    pageLength: 10,
                    info: true,
                    paging: true,
                    pagingType: "full_numbers",
                    processing: true,
                    responsive: true,
                    language: {
                        "decimal":           "",
                        "emptyTable":        "Nenhum registro encontrado",
                        "info":              "Exibindo _END_ de _TOTAL_ registros",
                        "infoEmpty":         "Exibindo nenhum registro",
                        "infoFiltered":      "- Filtrados de _MAX_ de registros",
                        "infoPostFix":       "",
                        "thousands":         ".",
                        "lengthMenu":        "Exibir _MENU_ registros",
                        "loadingRecords":    "Carregando...",
                        "processing":        "Processando...",
                        "search":            "_INPUT_",
                        "searchPlaceholder": "Pesquisar",
                        "zeroRecords":       "Nenhum registro correspondente encontrado",
                        "paginate": {
                            "first":         "<i class='fa fa-angle-double-left'></i>",
                            "last":          "<i class='fa fa-angle-double-right'></i>",
                            "next":          "<i class='fa fa-angle-right'>",
                            "previous":      "<i class='fa fa-angle-left'></i>"
                        }
                    }
                });
            }
        });
    });
}